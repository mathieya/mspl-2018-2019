---
title: "Analyse du fichier airmiles.csv"
author: "Yacine Mathieu"
date: "February 2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

#Jeu de donn�es
Analyse du fichier airmiles.csv
```{r}
df <- read.csv("data/airmiles.csv", header=TRUE);
head(df);
```

```{r}
library(ggplot2);
ggplot();
```

#Affichage des donn�es
On affiche un graphique les donn�es du fichier avec en x les ann�es et en y l'alitude de vol correspondant � chaque ann�e.
```{r}
ggplot(data=df, aes(x=time, y=airmiles)) +
    geom_point() +
    geom_line() +
    xlab("Ann�es") +
    ylab("Altitude de vol");
```

#Interpr�tation
On observe qu'au fil des ann�es, l'alitude de vol n'a cess� d'augmenter surtout � partir des ann�es 50, � l'apr�s guerre o� les �volutions technologiques ont permises des prouesses techniques de vol.
